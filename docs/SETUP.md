# Setting up the bot in your workspace

This guide will walk you through installing the GitLab Slack app in your workspace.

1. Make sure you have cloned the repository locally. Follow the steps in the [README](../README.md) on how to do that. For a quick start, follow the steps under "Making the bot available through Ngrok" as well.
2. Go to https://api.slack.com/apps and click "Create New App". Select "From scratch". Choose a friendly name for your app and select the workspace you want to install it in:

![Screenshot of creating a new app in Slack](./create-app.png)

3. Click "Create App".
4. Under "Basic information" > "Add features and functionality", click “Permissions”:

![Screenshot of the Basic Information page in Slack](./basic-info-permissions.png)

5. Scroll down to “Scopes”. Under "Bot Token Scopes", make sure to add `links:read` and `links:write`:

![Screenshot of permissions page in Slack](./permissions-scopes.png)

6. Scroll back up and click “Add to workspace”. This will generate a `Bot User OAuth Token`. In the repository you cloned locally, copy `.env.example` to `.env` (if you didn't already do that) and set the value of `SLACK_BOT_TOKEN` to this token.

7. Go back to the "Basic Information" page. Under "App Credentials", copy the value of "Signing Secret" and add it as `SLACK_SIGNING_SECRET` to your `.env` file.

8. In GitLab, create a [Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens) with the `read_api` scope. Add it as `GITLAB_PAT` to your `.env` file.

9. Start the app locally by running `npm run start`. The environment variables you just entered should be loaded now. Open another terminal window and run `ngrok http 3000`. Copy the Ngrok HTTPS url you get in the terminal (`https://XXXX.ngrok.io`) - you'll need it in a second.

10. Go back to the "Basic Information" page in Slack's web interface. Under "Add features and functionality", click "Event Subscriptions".

![Screenshot of the Basic Information page in Slack](./basic-info-permissions.png)

11. Scroll down to "Subscribe to bot events". Make sure to add `link_shared`.

![Screenshot of the Event Subscriptions page in Slack](./event-subscriptions.png)

12. Scroll back up. Under "Enable Events", enter your Ngrok URL. Make sure it ends with `/slack/events`, as that's the endpoint for event listeners:

![Screenshot of the Event Subscriptions page in Slack](./event-subscriptions-url.png)

13. Click "Save changes" at the bottom of the page. You might be asked to reinstall the app in your workspace due to the new permissions.

14. That's it! You now have a working GitLab Slack app :) If you're ready to deploy an actual AWS Lambda, head back to [README.md](../README.md) and follow the instructions there.