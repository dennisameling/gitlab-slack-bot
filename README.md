# GitLab Slack bot

This bot is used for generating GitLab URL previews, as they don't support it themselves yet: https://gitlab.com/gitlab-org/gitlab/-/issues/14194

![Screenshot of URL preview example](./gitlab-example-url-preview.png)

**Want to install the app in your Slack workspace? Head over to [SETUP.md](docs/SETUP.md)!**

## Local development

1. Copy `.env.example` to `.env`
1. Add all the necessary credentials to `.env`
1. Run `npm install` to install the dependencies
1. Run `npx serverless offline --noPrependStageInUrl` to start the development server

## Making the bot available through Ngrok

This is useful when developing in the app and you need to test things like the event listener on a publicly available URL.

1. `brew install ngrok`
1. `npx serverless offline --noPrependStageInUrl`
1. (separate terminal window) `ngrok http 3000`
1. The app is now available at `https://YOUR_SUBDOMAIN.ngrok.io` which can be used for testing purposes

## Deploying new versions to your workspace

We currently only support AWS Lambda for deployments.
You basically have two options for deploying new versions:

1. Locally through your terminal
2. Through GitLab CI

### Local depoyment through terminal

When you're ready to deploy, simply run `npx serverless deploy` in your terminal. The app will now be deployed (or updated) in AWS Lambda. For more details, [please refer to the serverless documentation](https://www.serverless.com/framework/docs/providers/aws/cli-reference/deploy).

### Deployment through GitLab CI

A pipeline is already available in the `.gitlab-ci.yml` file. Please make sure you set `GITLAB_PAT`, `SLACK_BOT_TOKEN` and `SLACK_SIGNING_SCRET` in your CI/CD variables.

## Contributing

Contributions of all types are welcome! Please feel free to create an issue or MR - I'll happily review those.
