import axios, { AxiosRequestConfig } from 'axios'
import { MessageAttachment } from '@slack/types'
import { MergeRequest } from './gitlab.types';
import { Logger } from '@slack/logger';

type UrlType = {
  regex: RegExp
  callback: (originalUrl: string, matches: RegExpMatchArray) => Promise<MessageAttachment | null>
}

export class GitLabService {
  private axiosConfig: AxiosRequestConfig;
  private logger: Logger;

  constructor(logger: Logger, gitLabPat: string) {
    this.logger = logger;
    this.axiosConfig = {
      headers: {
        Authorization: `Bearer ${gitLabPat}`,
        Accept: 'application/json'
      }
    }
  }
  parseUrl(url: string): Promise<MessageAttachment | null> | null {
    const urlTypes: UrlType[] = [
      {
        // Example: https://gitlab.com/dennisameling/gitlab-slack-bot/-/merge_requests/1
        regex: /^https\:\/\/gitlab\.com\/(.*)\/-\/merge_requests\/(\d+)/,
        callback: (url, matches) => this.parseMergeRequest(url, matches[1], matches[2])
      },
      {
        regex: /^.*$/,
        callback: () => this.unsupportedUrl()
      }
    ]

    for (const type of urlTypes) {
      const match = url.match(type.regex)
      if (match) {
        this.logger.debug(`Found URL match! ${url}`)
        return type.callback(url, match)
      }
    }

    return null
  }

  private parseMergeRequest(
    originalUrl: string,
    projectPath: string,
    mergeRequestIid: string
  ): Promise<MessageAttachment | null> {
    const encodedProjectPath = encodeURIComponent(projectPath)
    this.logger.debug(`Parsing MR ${projectPath}, ID ${mergeRequestIid}`)

    return axios.get<MergeRequest>(
      `https://gitlab.com/api/v4/projects/${encodedProjectPath}/merge_requests/${mergeRequestIid}`,
      this.axiosConfig
    ).then(({ data }): MessageAttachment => ({
      blocks: [
        {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text: `*<${originalUrl}|#${data.iid} ${data.title}>*\n*Author*: ${data.author.name}\n*Description*:\n${data.description}`
          }
        }
      ]
    })).catch(e => {
      this.logger.error('Error while contacting GitLab', e.toJSON())
      return null
    })
  }

  private unsupportedUrl(): Promise<MessageAttachment> {
    return new Promise((res) => {
      res({
        blocks: [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: `We don't yet support this GitLab URL type for generating previews. Feel free to <https://gitlab.com/dennisameling/gitlab-slack-bot|contribute it>!`
            }
          }
        ]
      })
    })
  }
}
