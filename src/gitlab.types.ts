export interface MergeRequest {
    /** Globally unique ID of the MR. Generally a large number. */
    id: number;
    /** ID of the MR within the project (starts at 1) */
    iid: number;
    project_id: number;
    title: string;
    description: string;
    author: MergeRequestAuthor;
    state: string;
}

export interface MergeRequestAuthor {
    id: string;
    name: string;
    username: string;
}
