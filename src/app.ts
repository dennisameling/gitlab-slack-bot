import { App, LinkUnfurls, LogLevel, AwsLambdaReceiver } from '@slack/bolt';
import { GitLabService } from './gitlab.service';
import initEnv from './env.helper';

initEnv()

const awsLambdaReceiver = new AwsLambdaReceiver({
  signingSecret: process.env.SLACK_SIGNING_SECRET || '',
});

const app = new App({
  token: process.env.SLACK_BOT_TOKEN,
  receiver: awsLambdaReceiver,
  logLevel: LogLevel.INFO
});

app.event('link_shared', async (data) => {
  const service = new GitLabService(data.logger, process.env.GITLAB_PAT || '')
  const finalUrls: LinkUnfurls = {}

  // A single Slack message can have multiple URLs, so we have to loop through all of them.
  for (const link of data.body.event.links) {
    const result = await service.parseUrl(link.url)

    if (!result) {
      finalUrls[link.url] = {
        blocks: [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: `An unknown error occurred while generating the URL preview.`
            }
          }
        ]
      }
      return
    }

    finalUrls[link.url] = result
  }

  await app.client.chat.unfurl({
    token: app.client.token,
    channel: data.event.channel,
    ts: data.event.message_ts,
    unfurls: finalUrls
  })
});

// Handle the Lambda function event
module.exports.handler = async (event, context, callback) => {
  const handler = await awsLambdaReceiver.start();
  return handler(event, context, callback);
}
