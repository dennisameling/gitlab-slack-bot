import { resolve } from 'path';
import { config } from 'dotenv';

const init = () => {
    const pathToConfig = '../.env';
    config({ path: resolve(__dirname, pathToConfig) });

    if (!process.env.SLACK_BOT_TOKEN) {
        throw Error('SLACK_BOT_TOKEN is missing in the environment variables!')
    }

    if (!process.env.SLACK_SIGNING_SECRET) {
        throw Error('SLACK_SIGNING_SECRET is missing in the environment variables!')
    }

    if (!process.env.GITLAB_PAT) {
        throw Error('GITLAB_PAT is missing in the environment variables!')
    }
}

export default init
